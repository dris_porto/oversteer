
#socket_echo_client.py


import socket
import sys


def initToDriS():
	# Create a TCP/IP socket
	sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

	# Connect the socket to the port where the server is listening
	server_address = ('localhost', 10000)

	print('connecting to {} port {}'.format(*server_address))
	sock.connect(server_address)	
	return sock
	

def sendToDriS(sock, message):
	sock.sendall(message)

    

def socketClose(sock):
	print('closing socket')
	sock.close()


#sock=initToDriS()

#sendToDriS(sock, b'ola')

#socketClose(sock);

