
#socket_echo_server.py

import socket
import sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

# Bind the socket to the port
server_address = ('localhost', 10000)

print('starting up on {} port {}'.format(*server_address))
sock.bind(server_address)


# Receive the data in small chunks and retransmit it
while True:
    data = sock.recv(255)
    print('received {!r}'.format(data))
   


# Clean up the connection
sock.close()


